#include "configuration.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* wifi_network = WIFI_NETWORK;
const char* wifi_pass = WIFI_PASSWORD;
const char* mqtt_serv_address = MQTT_SERVER_ADDRESS;
const char* mqtt_user = MQTT_USER; 
const char* mqtt_passwd = MQTT_PASSWORD; 
const int mqtt_port_number = MQTT_PORT_NUMBER;

long lastMsg = 0;
int moist = 0; 
#define MOIST_SENSOR A0

WiFiClient espClient;
PubSubClient client(espClient);

void setup() { 
  Serial.begin(115200); 
  setup_wifi(); 
  client.setServer(mqtt_serv_address, mqtt_port_number); 
} 

void setup_wifi() {
  delay(10); 
  // We start by connecting to a WiFi network 
  Serial.println(); 
  Serial.print("Connecting to "); 
  Serial.println(wifi_network); 
   
  WiFi.begin(wifi_network, wifi_pass); 
   
  while (WiFi.status() != WL_CONNECTED) { 
    WiFi.begin(wifi_network, wifi_pass); 
     
    Serial.print("."); 
    delay(5000); 
  } 
   
  Serial.println(""); 
  Serial.println("WiFi connected"); 
  Serial.println("IP address: "); 
  Serial.println(WiFi.localIP()); 
} 

void reconnect() { 
  // Loop until we're reconnected 
  while (!client.connected()) { 
    Serial.print("Attempting MQTT connection..."); 
    // Attempt to connect 
    if (client.connect("ESP8266Client", mqtt_user, mqtt_passwd)) { 
        Serial.println("connected"); 
    } else { 
      Serial.print("failed, rc="); 
      Serial.print(client.state()); 
      Serial.println(" try again in 5 seconds"); 
      // Wait 5 seconds before retrying 
      delay(5000); 
    } 
  } 
} 

void loop() { 
 
  if (!client.connected()) { 
    reconnect(); 
  } 
  client.loop(); 

  // Read the capacitive moist sensor
  moist = analogRead(MOIST_SENSOR); 
  Serial.println(moist); 

  // Send via MQTT
  client.publish("moes/moist", String(moist).c_str()); 
  
  delay(1000);
  
} 

